// user.dart
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'container_model.freezed.dart';

@freezed
abstract class ContainerModel with _$ContainerModel {
  const factory ContainerModel({
    @Default(100.0) double containerWidth,
    @Default(100.0) double containerHeight,
  }) = _ContainerModel;
}