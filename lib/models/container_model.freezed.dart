// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'container_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ContainerModel {
  double get containerWidth => throw _privateConstructorUsedError;
  double get containerHeight => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ContainerModelCopyWith<ContainerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContainerModelCopyWith<$Res> {
  factory $ContainerModelCopyWith(
          ContainerModel value, $Res Function(ContainerModel) then) =
      _$ContainerModelCopyWithImpl<$Res, ContainerModel>;
  @useResult
  $Res call({double containerWidth, double containerHeight});
}

/// @nodoc
class _$ContainerModelCopyWithImpl<$Res, $Val extends ContainerModel>
    implements $ContainerModelCopyWith<$Res> {
  _$ContainerModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? containerWidth = null,
    Object? containerHeight = null,
  }) {
    return _then(_value.copyWith(
      containerWidth: null == containerWidth
          ? _value.containerWidth
          : containerWidth // ignore: cast_nullable_to_non_nullable
              as double,
      containerHeight: null == containerHeight
          ? _value.containerHeight
          : containerHeight // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ContainerModelCopyWith<$Res>
    implements $ContainerModelCopyWith<$Res> {
  factory _$$_ContainerModelCopyWith(
          _$_ContainerModel value, $Res Function(_$_ContainerModel) then) =
      __$$_ContainerModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double containerWidth, double containerHeight});
}

/// @nodoc
class __$$_ContainerModelCopyWithImpl<$Res>
    extends _$ContainerModelCopyWithImpl<$Res, _$_ContainerModel>
    implements _$$_ContainerModelCopyWith<$Res> {
  __$$_ContainerModelCopyWithImpl(
      _$_ContainerModel _value, $Res Function(_$_ContainerModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? containerWidth = null,
    Object? containerHeight = null,
  }) {
    return _then(_$_ContainerModel(
      containerWidth: null == containerWidth
          ? _value.containerWidth
          : containerWidth // ignore: cast_nullable_to_non_nullable
              as double,
      containerHeight: null == containerHeight
          ? _value.containerHeight
          : containerHeight // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_ContainerModel
    with DiagnosticableTreeMixin
    implements _ContainerModel {
  const _$_ContainerModel(
      {this.containerWidth = 100.0, this.containerHeight = 100.0});

  @override
  @JsonKey()
  final double containerWidth;
  @override
  @JsonKey()
  final double containerHeight;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ContainerModel(containerWidth: $containerWidth, containerHeight: $containerHeight)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ContainerModel'))
      ..add(DiagnosticsProperty('containerWidth', containerWidth))
      ..add(DiagnosticsProperty('containerHeight', containerHeight));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ContainerModel &&
            (identical(other.containerWidth, containerWidth) ||
                other.containerWidth == containerWidth) &&
            (identical(other.containerHeight, containerHeight) ||
                other.containerHeight == containerHeight));
  }

  @override
  int get hashCode => Object.hash(runtimeType, containerWidth, containerHeight);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ContainerModelCopyWith<_$_ContainerModel> get copyWith =>
      __$$_ContainerModelCopyWithImpl<_$_ContainerModel>(this, _$identity);
}

abstract class _ContainerModel implements ContainerModel {
  const factory _ContainerModel(
      {final double containerWidth,
      final double containerHeight}) = _$_ContainerModel;

  @override
  double get containerWidth;
  @override
  double get containerHeight;
  @override
  @JsonKey(ignore: true)
  _$$_ContainerModelCopyWith<_$_ContainerModel> get copyWith =>
      throw _privateConstructorUsedError;
}
