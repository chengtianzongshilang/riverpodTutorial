import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final containerHeightProvider = StateProvider((ref) => 100.0);
final containerWidthProvider = StateProvider((ref) => 100.0);




class PageWithRiverpod extends ConsumerWidget {
  const PageWithRiverpod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double containerHeightValue = ref.watch(containerHeightProvider);
    double containerWidthValue = ref.watch(containerWidthProvider);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ref.refresh(containerHeightProvider);
          ref.refresh(containerWidthProvider);

        },
        child: const Icon(Icons.ac_unit_outlined),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                    color: CupertinoColors.white,
                    child: Center(
                      child: Image.asset('assets/images/seiya.jpg',width: containerWidthValue, height: containerHeightValue,fit: BoxFit.fill),
                    ),
                  )),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Width: ${containerWidthValue.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: containerWidthValue,
                          onChanged: (double value) {
                            ref.read(containerWidthProvider.notifier).state = value;
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Height: ${containerHeightValue.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: containerHeightValue,
                          onChanged: (double value) {
                            ref.read(containerHeightProvider.notifier).state = value;
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}