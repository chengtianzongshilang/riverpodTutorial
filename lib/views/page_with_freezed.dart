import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_tutorial/view_models/container_view_model.dart';


class PageWithFreezed extends ConsumerWidget {
  const PageWithFreezed({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final containerState = ref.watch(containerProvider);
    final containerNotifier = ref.watch(containerProvider.notifier);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ref.refresh(containerProvider);
        },
        child: const Icon(Icons.ac_unit_outlined),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                    color: CupertinoColors.white,
                    child: Center(
                      child: Image.asset('assets/images/seiya.jpg',width: containerState.containerWidth, height: containerState.containerHeight,fit: BoxFit.fill),
                    ),
                  )),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Width: ${containerState.containerWidth.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: containerState.containerWidth,
                          onChanged: (double value) {
                            containerNotifier.changeWidth(value);
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Height: ${containerState.containerHeight.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: containerState.containerHeight,
                          onChanged: (double value) {
                            containerNotifier.changeHeight(value);
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}