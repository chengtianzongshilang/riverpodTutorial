import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';



class PageWithStateful extends StatefulWidget {
  const PageWithStateful({Key? key}) : super(key: key);

  @override
  State<PageWithStateful> createState() => _PageWithStateful();
}

class _PageWithStateful extends State<PageWithStateful> {
  double _containerWidthValue = 0.0;
  double _containerHeightValue = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _containerHeightValue = 0;
            _containerWidthValue = 0;
          });
          },
    child: const Icon(Icons.ac_unit_outlined),
        ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                color: CupertinoColors.white,
                child: Center(
                  child: Image.asset('assets/images/seiya.jpg',width: _containerWidthValue, height: _containerHeightValue,fit: BoxFit.fill),
                ),
              )),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Width: ${_containerWidthValue.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: _containerWidthValue,
                          onChanged: (value) {
                            setState(() {
                              _containerWidthValue = value;
                            });
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Row(
                  children: [
                    SizedBox(
                      width: 80,
                      child: Text(
                        'Height: ${_containerHeightValue.toStringAsFixed(0)}',
                      ),
                    ), // Sliderの値を表示),
                    Expanded(
                      child: SizedBox(
                        child: Slider(
                          value: _containerHeightValue,
                          onChanged: (value) {
                            setState(() {
                              _containerHeightValue = value;
                            });
                          },
                          min: 0.0,
                          max: 300.0,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}