import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_tutorial/models/container_model.dart';

class ContainerNotifier extends StateNotifier<ContainerModel>{
  ContainerNotifier() : super(
    const ContainerModel()
  );
  void changeWidth(value) async {
    state = state.copyWith(
      containerWidth: value
    );
  }
  void changeHeight(value) async {
    state = state.copyWith(
        containerHeight: value
    );
  }
}



final containerProvider = StateNotifierProvider<ContainerNotifier, ContainerModel>((ref) {
  return ContainerNotifier();
});