import 'dart:core';

import 'package:flutter/material.dart';
import 'package:riverpod_tutorial/views/menu_page.dart';
import 'package:riverpod_tutorial/views/page_with_freezed.dart';
import 'package:riverpod_tutorial/views/page_with_riverpod.dart';
import 'package:riverpod_tutorial/views/page_with_stateful.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';




void main(){
  runApp(const ProviderScope(child: MyApp()));
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {
        '/01': (context) => const PageWithStateful(),
        '/02': (context) => const PageWithRiverpod(),
        '/03': (context) => const PageWithFreezed(),


      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MenuPage(),
    );
  }
}